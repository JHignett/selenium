package selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Element {
	
	Constants constants;

	private final String landingUrl = constants.getUrl();
	private final String user = constants.getUser();
	private final String password = constants.getPassword();

	@FindBy(id="")
	WebElement textfieldUsername;
	
	@FindBy(xpath = "")
	WebElement textfieldPassword;
	
	@FindBy(xpath = "")
	WebElement buttonSubmit;

	public void attemptLogin(WebDriver driver) {
		textfieldUsername.sendKeys(user);
		textfieldPassword.sendKeys(password);
		buttonSubmit.click();
	}

	public String getLandingUrl() {
		return landingUrl;
	}
}
