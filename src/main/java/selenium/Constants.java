package selenium;

public class Constants {
	private final String url = "";
	private final String user = "";
	private final String password = "";
	private final String driverPath = "";
	
	
	public String getUrl() {
		return url;
	}
	public String getUser() {
		return user;
	}
	public String getPassword() {
		return password;
	}
	
	public String getDriverPath() {
		return driverPath;
	}
	
}
