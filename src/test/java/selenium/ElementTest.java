package selenium;

import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

public class ElementTest {
	
	WebDriver driver;
	Element element;
	Constants constants;
	
	@Before
	public void setup() {
		System.setProperty("webdriver.chrome.driver",constants.getDriverPath());
		driver = new ChromeDriver();
		driver.get(constants.getUrl());
		element = PageFactory.initElements(driver, Element.class);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	@AfterClass
	public void teardown() {
		driver.close();
	}

	@Test
	public void test() {
		assertTrue(true);
	}
	
}
